const Number = require('./Number');
const CallFactory = require('./CallFactory');
const Bridge = require('./Bridge');
const debug = require('debug')('Dialer:Dialer');

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitUntilIsNotPending(bridge) {
    do {
        var bridgeStatus = await bridge.getStatus();
        debug('check status', bridgeStatus);
        if(bridgeStatus === bridge.STATUSES.PENDING) {
            await timeout(500);
        }
    } while(bridgeStatus === bridge.STATUSES.PENDING)
    return bridgeStatus;
}

async function smartBridge(bridge) {
    var isNotPendingStatus = await waitUntilIsNotPending(bridge);
    if(isNotPendingStatus === bridge.STATUSES.READY) {
        return bridge.bridge()
        .catch(function(error) {
            debug('catch', error);
            return false;
        });
    }
    return false;
}

function Dialer(connector) {
    if (typeof connector !== 'object' || connector.constructor.name !== 'Connector') {
        throw 'connector must be an instance of Connector';
    }
    let _connector = connector;
    let _callFactory = new CallFactory(_connector);

    this.call = async function(number1, number2) {
        [call1, call2] = await Promise.all([_callFactory.create(number1), _callFactory.create(number2)])
        let bridge = new Bridge(call1, call2, connector);
        var bridgeResult = smartBridge(bridge);
        return bridge;
    }
}

module.exports = Dialer;