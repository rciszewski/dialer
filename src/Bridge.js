const Call = require('./Call');
const Connector = require('./Connector');
const debug = require('debug')('Dialer:Bridge');
const STATUSES = {
    NEW: "NEW",
    READY: "READY",
    PENDING: "PENDING",
    BRIDGED: "BRIDGED",
    FINISHED: "FINISHED",
    FAILED: "FAILED"
}
function callIsValid(call) {
    return call instanceof Call;
}

function canBridge(call1, call2) {
    return getStatus(call1, call2)
    .then(function(status) {
        return status === STATUSES.READY;
    });
}

function getStatus(call1, call2) {
    return Promise.all([call1.getStatus(), call2.getStatus()])
        .then(function ([status1, status2]) {
            let isFailed = ~['BUSY', 'FAILED', 'NO ANSWER'].indexOf(status1) || ~['BUSY', 'FAILED', 'NO ANSWER'].indexOf(status2)
            let isFinished = !~['RINGING', 'CONNECTED'].indexOf(status1) || !~['RINGING', 'CONNECTED'].indexOf(status2);
            let isReady = status1 === status2 && status1 === 'CONNECTED';
            switch (true) {
                case isFailed:
                    return STATUSES.FAILED
                case isFinished:
                    return STATUSES.FINISHED
                case isReady:
                    return STATUSES.READY
                default:
                    return STATUSES.PENDING
            }
        })
}

function Bridge(call1, call2, connector) {
    if(!callIsValid(call1) || !callIsValid(call2)) {
        throw 'call1 and call2 must be an instances of Call';
    }
    if(!connector || !connector instanceof Connector) {
        throw 'connector must be an instance of Connector';
    }
    var _call1 = call1;
    var _call2 = call2;
    var _connector = connector;
    var _isBridged = false;
    var _lastStatus = null;

    this.STATUSES = STATUSES;
    this.bridge = function() {
        return this.canBridge().then(function (result) {
            debug('czy moge mostowac?', result);
            if (result === true) {
                return _connector.bridge(_call1.getId(), _call2.getId())
                .then(function() {
                    _isBridged = true;
                    debug('Zmostowano pomyślne',_call1.getId(),_call2.getId());
                    return true;
                });
            }
            throw 'cannot bridge'
        })
    }

    this.getStatus = async function() {
        if (~[STATUSES.FINISHED, STATUSES.FAILED].indexOf(_lastStatus)) {
            return _lastStatus;
        }
        _lastStatus = await getStatus(_call1, _call2);
        if (_lastStatus === STATUSES.READY && _isBridged) {
            _lastStatus = STATUSES.BRIDGED;
        }
        return _lastStatus;
    }

    this.canBridge = function() {
        return canBridge(_call1, _call2);
    }
}

module.exports = Bridge;