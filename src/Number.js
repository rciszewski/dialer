var isNumberValid = function (number) {
    if (!number) {
        return false;
    }
    try {
        number = number.toString();
    } catch (e) {
        return false;
    }
    if (number.length !== 9) {
        return false;
    }
    if (parseInt(number, 10).toString() !== number) {
        return false;
    }
    return true;
}

function Number(number) {
    if (!isNumberValid(number)) {
        throw 'bad number ' + number;
    }
    number = number.toString();
    let _number = number;

    this.toString = function () {
        return _number;
    }

    this.getNumber = function() {
        return _number;
    }
}

module.exports = Number;