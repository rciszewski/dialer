# Dialer
## Instalacja
package.json
```javascript
{
    "dialer": "/url/do/dialera/dostarczonego/przez/nas"
}
```
## Komponenty
#### Connector
Connector jest klasą, która służy do komunikacji z restowym API do wydzwaniania
##### API
- `constructor(<string> url, <string> login, <string> password)`
- `call(<string> number) : Promise <json>`
- `getStatus(<string> callId) : Promise <json>`
- `bridge(<string> callId1, <string> callId2) : promise <Bridge>`
##### Przykład użycia
```javascript
let login = '<login>'
let password = '<pass>'
let url = '<url>'
const Connector = require('dialer').Connector
let connector = new Connector(url, login, password) 
let result = await connector.call('<number>')
let callId = result.id
```
---
#### CallFactory
CallFactory jest klasą służącą do dzwonienia
##### API
- `constructor(<Connector> connector)`
- `create(<string> number) : Promise <Call>`
##### Przykład użycia
```javascript
const Connector = require('dialer').Connector
const CallFactory = require('dialer').CallFactory
let connector = new Connector(<string> url, <string>login, <string> password) 
let callFactory = new CallFactory(connector)
let call = await CallFactory.create('<number>')
let status = call.getStatus() //RINGING
```
---
#### Call
##### API

- `getStatus() : Promise <string> [RINGING, CONNECTED, ANSWERED, FAILED, NO ANSWER, BUSY]`
- `getId() : Promise <string> id`

#### Bridge
##### API
- `constructor(<Call> call1, <Call> call2, <Connector> connector)`
- `bridge() : <Promise> true | throw Exception`
- `getStatus() : <Promise> <string> [NEW, READY, PENDING, BRIDGED, FINISHED]`
- `canBridge(): <Promise> <boolean>`
##### CONST
- `STATUSES: [NEW, READY, PENDING, BRIDGED, FINISHED]`

```javascript
let bridge = new Bridge(call1, call2, connector)
console.log(bridge.STATUSES.READY)
```

##### Przykład użycia 1
```javascript
const Connector = require('dialer').Connector
const CallFactory = require('dialer').CallFactory
const Bridge = require('dialer').Bridge

let connector = new Connector(<string> url, <string>login, <string> password)
let callFactory = new CallFactory(connector)
let call1 = await CallFactory.create('<number1>')
let call2 = await CallFactory.create('<number2>')
let bridge = new Bridge(call1, call2, connector)

let canBridge = await bridge.canBridge()
if (canBridge) {
  bridge.bridge()
}
```
##### Przykład użycia 2
```javascript
const Connector = require('dialer').Connector
const CallFactory = require('dialer').CallFactory
const Bridge = require('dialer').Bridge

let connector = new Connector(<string> url, <string>login, <string> password)
let callFactory = new CallFactory(connector)
let call1 = await CallFactory.create('<number1>')
let call2 = await CallFactory.create('<number2>')
let bridge = new Bridge(call1, call2, connector)

let bridgeStatus = await bridge.getStatus()
if (bridgeStatus = bridge.STATUSES.READY) {
  bridge.bridge()
}
```
---
#### Dialer
##### API
- `constructor(<connector> connector)`
- `call(<string> nubmer1, <string> number2) : <promise> Bridge`
##### Przykład użycia
```javascript
const Connector = require('dialer').Connector
const Dialer = require('dialer').Dialer;
let connector = new Connector(<string> url, <string>login, <string> password)
let dialer = new Dialer(connector)
let bridge = await dialer.call('<number1>','<number2>')
let status = await bridge.getStatus()
```
